<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ApiResource
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="game_master")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=70)
     */
    private $password;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Adventure", inversedBy="users")
     * @ApiSubresource(maxDepth=1)
     */
    private $adventures;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Party", mappedBy="gameMaster", orphanRemoval=true)
     */
    private $parties;

    /**
     * @ORM\Column(type="boolean")
     */
    private $betaTester;

    public function __construct()
    {
        $this->adventures = new ArrayCollection();
        $this->parties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles($roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
    * Returns the salt that was originally used to encode the password.
    *
    * This can return null if the password was not encoded using a salt.
    *
    * @return string|null The salt
    */
   public function getSalt()
   {
       return null;
   }

   /**
    * Returns the username used to authenticate the user.
    *
    * @return string The username
    */
   public function getUsername()
   {
       return $this->email;
   }

   /**
    * Removes sensitive data from the user.
    *
    * This is important if, at any given point, sensitive information like
    * the plain-text password is stored on this object.
    */
   public function eraseCredentials()
   {
       return null;
   }

   /**
    * @return Collection|adventure[]
    */
   public function getAdventures(): Collection
   {
       return $this->adventures;
   }

   public function addAdventure(adventure $adventure): self
   {
       if (!$this->adventures->contains($adventure)) {
           $this->adventures[] = $adventure;
       }

       return $this;
   }

   public function removeAdventure(adventure $adventure): self
   {
       if ($this->adventures->contains($adventure)) {
           $this->adventures->removeElement($adventure);
       }

       return $this;
   }

   /**
    * @return Collection|Party[]
    */
   public function getParties(): Collection
   {
       return $this->parties;
   }

   public function addParty(Party $party): self
   {
       if (!$this->parties->contains($party)) {
           $this->parties[] = $party;
           $party->setGameMaster($this);
       }

       return $this;
   }

   public function removeParty(Party $party): self
   {
       if ($this->parties->contains($party)) {
           $this->parties->removeElement($party);
           // set the owning side to null (unless already changed)
           if ($party->getGameMaster() === $this) {
               $party->setGameMaster(null);
           }
       }

       return $this;
   }

   public function getBetaTester(): ?bool
   {
       return $this->betaTester;
   }

   public function setBetaTester(bool $betaTester): self
   {
       $this->betaTester = $betaTester;

       return $this;
   }
}
