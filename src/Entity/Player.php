<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Character")
     * @ORM\JoinColumn(nullable=false)
     */
    private $character;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $inventory = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $alteration = [];

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Sharing", mappedBy="players")
     */
    private $sharings;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Party", inversedBy="players")
     * @ORM\JoinColumn(nullable=false)
     */
    private $party;

    public function __construct()
    {
        $this->sharings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getCharacter(): ?Character
    {
        return $this->character;
    }

    public function setCharacter(?Character $character): self
    {
        $this->character = $character;

        return $this;
    }

    public function getInventory(): ?array
    {
        return $this->inventory;
    }

    public function setInventory(?array $inventory): self
    {
        $this->inventory = $inventory;

        return $this;
    }

    public function getAlteration(): ?array
    {
        return $this->alteration;
    }

    public function setAlteration(?array $alteration): self
    {
        $this->alteration = $alteration;

        return $this;
    }

    /**
     * @return Collection|Sharing[]
     */
    public function getSharings(): Collection
    {
        return $this->sharings;
    }

    public function addSharing(Sharing $sharing): self
    {
        if (!$this->sharings->contains($sharing)) {
            $this->sharings[] = $sharing;
            $sharing->addPlayer($this);
        }

        return $this;
    }

    public function removeSharing(Sharing $sharing): self
    {
        if ($this->sharings->contains($sharing)) {
            $this->sharings->removeElement($sharing);
            $sharing->removePlayer($this);
        }

        return $this;
    }

    public function getParty(): ?Party
    {
        return $this->party;
    }

    public function setParty(?Party $party): self
    {
        $this->party = $party;

        return $this;
    }
}
