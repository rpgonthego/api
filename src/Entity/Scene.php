<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\SceneRepository")
 */
class Scene
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $illustration;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Adventure", inversedBy="scenes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $adventure;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Character", inversedBy="scenes")
     */
    private $Characters;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\GameAid", inversedBy="scenes")
     */
    private $gameAids;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $gm_information;

    /**
     * @ORM\Column(type="integer")
     */
    private $number;

    public function __construct()
    {
        $this->Characters = new ArrayCollection();
        $this->gameAids = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(string $illustration): self
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getAdventure(): ?Adventure
    {
        return $this->adventure;
    }

    public function setAdventure(?Adventure $adventure): self
    {
        $this->adventure = $adventure;

        return $this;
    }

    /**
     * @return Collection|Character[]
     */
    public function getCharacters(): Collection
    {
        return $this->Characters;
    }

    public function addCharacter(Character $character): self
    {
        if (!$this->Characters->contains($character)) {
            $this->Characters[] = $character;
        }

        return $this;
    }

    public function removeCharacter(Character $character): self
    {
        if ($this->Characters->contains($character)) {
            $this->Characters->removeElement($character);
        }

        return $this;
    }

    /**
     * @return Collection|GameAid[]
     */
    public function getGameAids(): Collection
    {
        return $this->gameAids;
    }

    public function addGameAid(GameAid $gameAid): self
    {
        if (!$this->gameAids->contains($gameAid)) {
            $this->gameAids[] = $gameAid;
        }

        return $this;
    }

    public function removeGameAid(GameAid $gameAid): self
    {
        if ($this->gameAids->contains($gameAid)) {
            $this->gameAids->removeElement($gameAid);
        }

        return $this;
    }

    public function getGmInformation(): ?string
    {
        return $this->gm_information;
    }

    public function setGmInformation(?string $gm_information): self
    {
        $this->gm_information = $gm_information;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }
}
