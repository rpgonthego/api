<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\AdventureRepository")
 */
class Adventure
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $illustration;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $theme;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Author;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="adventures")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Character", mappedBy="adventure", orphanRemoval=true)
     * @ApiSubresource(maxDepth=1)
     */
    private $characters;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GameAid", mappedBy="adventure", orphanRemoval=true)
     */
    private $gameAids;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Scene", mappedBy="adventure", orphanRemoval=true)
     */
    private $scenes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $beta;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->characters = new ArrayCollection();
        $this->gameAids = new ArrayCollection();
        $this->scenes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(string $illustration): self
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getTheme(): ?string
    {
        return $this->theme;
    }

    public function setTheme(string $theme): self
    {
        $this->theme = $theme;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->Author;
    }

    public function setAuthor(string $Author): self
    {
        $this->Author = $Author;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addAdventure($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeAdventure($this);
        }

        return $this;
    }

    /**
     * @return Collection|Character[]
     */
    public function getCharacters(): Collection
    {
        return $this->characters;
    }

    public function addCharacter(Character $character): self
    {
        if (!$this->characters->contains($character)) {
            $this->characters[] = $character;
            $character->setAdventure($this);
        }

        return $this;
    }

    public function removeCharacter(Character $character): self
    {
        if ($this->characters->contains($character)) {
            $this->characters->removeElement($character);
            // set the owning side to null (unless already changed)
            if ($character->getAdventure() === $this) {
                $character->setAdventure(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GameAid[]
     */
    public function getGameAids(): Collection
    {
        return $this->gameAids;
    }

    public function addGameAid(GameAid $gameAid): self
    {
        if (!$this->gameAids->contains($gameAid)) {
            $this->gameAids[] = $gameAid;
            $gameAid->setAdventure($this);
        }

        return $this;
    }

    public function removeGameAid(GameAid $gameAid): self
    {
        if ($this->gameAids->contains($gameAid)) {
            $this->gameAids->removeElement($gameAid);
            // set the owning side to null (unless already changed)
            if ($gameAid->getAdventure() === $this) {
                $gameAid->setAdventure(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Scene[]
     */
    public function getScenes(): Collection
    {
        return $this->scenes;
    }

    public function addScene(Scene $scene): self
    {
        if (!$this->scenes->contains($scene)) {
            $this->scenes[] = $scene;
            $scene->setAdventure($this);
        }

        return $this;
    }

    public function removeScene(Scene $scene): self
    {
        if ($this->scenes->contains($scene)) {
            $this->scenes->removeElement($scene);
            // set the owning side to null (unless already changed)
            if ($scene->getAdventure() === $this) {
                $scene->setAdventure(null);
            }
        }

        return $this;
    }

    public function getBeta(): ?bool
    {
        return $this->beta;
    }

    public function setBeta(bool $beta): self
    {
        $this->beta = $beta;

        return $this;
    }
}
