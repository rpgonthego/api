<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190706212747 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE sharing_player (sharing_id INT NOT NULL, player_id INT NOT NULL, PRIMARY KEY(sharing_id, player_id))');
        $this->addSql('CREATE INDEX IDX_99C7B38948F15050 ON sharing_player (sharing_id)');
        $this->addSql('CREATE INDEX IDX_99C7B38999E6F5DF ON sharing_player (player_id)');
        $this->addSql('ALTER TABLE sharing_player ADD CONSTRAINT FK_99C7B38948F15050 FOREIGN KEY (sharing_id) REFERENCES sharing (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sharing_player ADD CONSTRAINT FK_99C7B38999E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE player ADD party_id INT NOT NULL');
        $this->addSql('ALTER TABLE player ADD CONSTRAINT FK_98197A65213C1059 FOREIGN KEY (party_id) REFERENCES party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_98197A65213C1059 ON player (party_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE sharing_player');
        $this->addSql('ALTER TABLE player DROP CONSTRAINT FK_98197A65213C1059');
        $this->addSql('DROP INDEX IDX_98197A65213C1059');
        $this->addSql('ALTER TABLE player DROP party_id');
    }
}
