<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190706213217 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE scene ADD gm_information TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE sharing ADD character_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sharing ADD scene_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sharing ADD game_aid_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sharing ADD CONSTRAINT FK_AE81EC681136BE75 FOREIGN KEY (character_id) REFERENCES character (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sharing ADD CONSTRAINT FK_AE81EC68166053B4 FOREIGN KEY (scene_id) REFERENCES scene (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sharing ADD CONSTRAINT FK_AE81EC68B639B9DE FOREIGN KEY (game_aid_id) REFERENCES game_aid (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_AE81EC681136BE75 ON sharing (character_id)');
        $this->addSql('CREATE INDEX IDX_AE81EC68166053B4 ON sharing (scene_id)');
        $this->addSql('CREATE INDEX IDX_AE81EC68B639B9DE ON sharing (game_aid_id)');
        $this->addSql('ALTER TABLE "character" ADD gm_information TEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE sharing DROP CONSTRAINT FK_AE81EC681136BE75');
        $this->addSql('ALTER TABLE sharing DROP CONSTRAINT FK_AE81EC68166053B4');
        $this->addSql('ALTER TABLE sharing DROP CONSTRAINT FK_AE81EC68B639B9DE');
        $this->addSql('DROP INDEX IDX_AE81EC681136BE75');
        $this->addSql('DROP INDEX IDX_AE81EC68166053B4');
        $this->addSql('DROP INDEX IDX_AE81EC68B639B9DE');
        $this->addSql('ALTER TABLE sharing DROP character_id');
        $this->addSql('ALTER TABLE sharing DROP scene_id');
        $this->addSql('ALTER TABLE sharing DROP game_aid_id');
        $this->addSql('ALTER TABLE scene DROP gm_information');
        $this->addSql('ALTER TABLE character DROP gm_information');
    }
}
