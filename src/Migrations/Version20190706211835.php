<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190706211835 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE party_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE scene_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE sharing_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE party (id INT NOT NULL, game_master_id INT NOT NULL, adventure_id INT NOT NULL, name VARCHAR(150) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, last_played TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_89954EE0C1151A13 ON party (game_master_id)');
        $this->addSql('CREATE INDEX IDX_89954EE055CF40F9 ON party (adventure_id)');
        $this->addSql('CREATE TABLE scene (id INT NOT NULL, adventure_id INT NOT NULL, name VARCHAR(150) NOT NULL, description TEXT NOT NULL, illustration VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D979EFDA55CF40F9 ON scene (adventure_id)');
        $this->addSql('CREATE TABLE scene_character (scene_id INT NOT NULL, character_id INT NOT NULL, PRIMARY KEY(scene_id, character_id))');
        $this->addSql('CREATE INDEX IDX_FE1E8CBE166053B4 ON scene_character (scene_id)');
        $this->addSql('CREATE INDEX IDX_FE1E8CBE1136BE75 ON scene_character (character_id)');
        $this->addSql('CREATE TABLE scene_game_aid (scene_id INT NOT NULL, game_aid_id INT NOT NULL, PRIMARY KEY(scene_id, game_aid_id))');
        $this->addSql('CREATE INDEX IDX_EA367F7A166053B4 ON scene_game_aid (scene_id)');
        $this->addSql('CREATE INDEX IDX_EA367F7AB639B9DE ON scene_game_aid (game_aid_id)');
        $this->addSql('CREATE TABLE sharing (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE party ADD CONSTRAINT FK_89954EE0C1151A13 FOREIGN KEY (game_master_id) REFERENCES game_master (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE party ADD CONSTRAINT FK_89954EE055CF40F9 FOREIGN KEY (adventure_id) REFERENCES adventure (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE scene ADD CONSTRAINT FK_D979EFDA55CF40F9 FOREIGN KEY (adventure_id) REFERENCES adventure (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE scene_character ADD CONSTRAINT FK_FE1E8CBE166053B4 FOREIGN KEY (scene_id) REFERENCES scene (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE scene_character ADD CONSTRAINT FK_FE1E8CBE1136BE75 FOREIGN KEY (character_id) REFERENCES character (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE scene_game_aid ADD CONSTRAINT FK_EA367F7A166053B4 FOREIGN KEY (scene_id) REFERENCES scene (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE scene_game_aid ADD CONSTRAINT FK_EA367F7AB639B9DE FOREIGN KEY (game_aid_id) REFERENCES game_aid (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE scene_character DROP CONSTRAINT FK_FE1E8CBE166053B4');
        $this->addSql('ALTER TABLE scene_game_aid DROP CONSTRAINT FK_EA367F7A166053B4');
        $this->addSql('DROP SEQUENCE party_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE scene_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE sharing_id_seq CASCADE');
        $this->addSql('DROP TABLE party');
        $this->addSql('DROP TABLE scene');
        $this->addSql('DROP TABLE scene_character');
        $this->addSql('DROP TABLE scene_game_aid');
        $this->addSql('DROP TABLE sharing');
    }
}
