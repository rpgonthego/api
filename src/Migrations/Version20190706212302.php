<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190706212302 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE player_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE player (id INT NOT NULL, character_id INT NOT NULL, token VARCHAR(255) NOT NULL, inventory TEXT DEFAULT NULL, alteration TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_98197A651136BE75 ON player (character_id)');
        $this->addSql('COMMENT ON COLUMN player.inventory IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN player.alteration IS \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE player ADD CONSTRAINT FK_98197A651136BE75 FOREIGN KEY (character_id) REFERENCES character (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sharing ADD party_id INT NOT NULL');
        $this->addSql('ALTER TABLE sharing ADD CONSTRAINT FK_AE81EC68213C1059 FOREIGN KEY (party_id) REFERENCES party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_AE81EC68213C1059 ON sharing (party_id)');
        $this->addSql('ALTER TABLE "character" ADD hero BOOLEAN NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE player_id_seq CASCADE');
        $this->addSql('DROP TABLE player');
        $this->addSql('ALTER TABLE character DROP hero');
        $this->addSql('ALTER TABLE sharing DROP CONSTRAINT FK_AE81EC68213C1059');
        $this->addSql('DROP INDEX IDX_AE81EC68213C1059');
        $this->addSql('ALTER TABLE sharing DROP party_id');
    }
}
