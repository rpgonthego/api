<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190705225932 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE adventure_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE user_adventure (user_id INT NOT NULL, adventure_id INT NOT NULL, PRIMARY KEY(user_id, adventure_id))');
        $this->addSql('CREATE INDEX IDX_9E6503EBA76ED395 ON user_adventure (user_id)');
        $this->addSql('CREATE INDEX IDX_9E6503EB55CF40F9 ON user_adventure (adventure_id)');
        $this->addSql('CREATE TABLE adventure (id INT NOT NULL, name VARCHAR(200) NOT NULL, description TEXT NOT NULL, illustration VARCHAR(255) NOT NULL, theme VARCHAR(50) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, author VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE user_adventure ADD CONSTRAINT FK_9E6503EBA76ED395 FOREIGN KEY (user_id) REFERENCES game_master (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_adventure ADD CONSTRAINT FK_9E6503EB55CF40F9 FOREIGN KEY (adventure_id) REFERENCES adventure (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE user_adventure DROP CONSTRAINT FK_9E6503EB55CF40F9');
        $this->addSql('DROP SEQUENCE adventure_id_seq CASCADE');
        $this->addSql('DROP TABLE user_adventure');
        $this->addSql('DROP TABLE adventure');
    }
}
