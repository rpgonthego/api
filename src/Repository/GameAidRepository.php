<?php

namespace App\Repository;

use App\Entity\GameAid;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GameAid|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameAid|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameAid[]    findAll()
 * @method GameAid[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameAidRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GameAid::class);
    }

    // /**
    //  * @return GameAid[] Returns an array of GameAid objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GameAid
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
